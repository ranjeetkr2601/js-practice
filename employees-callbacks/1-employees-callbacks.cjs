/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/
const fs = require('fs');
const path = require('path');

function readFile(filePath, callback) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if(err){
            callback(err);
        }
        else {
            callback(null, JSON.parse(data));
        }
    });
}

function retrieveData(searchIds, callback){
    if(Array.isArray(searchIds)){
        readFile('data.json', (err, data) => {
            if(err){
                callback(err);
            } else { 
                let foundData = [];
                if('employees' in data){
                    foundData = data.employees.filter((currentEmployee) => {
                            return searchIds.includes(currentEmployee.id);
                        });
                }
                fs.writeFile(path.join(__dirname, 'problem1.json'), JSON.stringify(foundData, null, 4), (err) => {
                    if(err){
                        callback(err);
                    }
                    else{
                        callback(null);
                    }
                });
            }
        });
    } 
    else {
        callback("searchIds is not an Array");
    }
}

function groupByCompanies(callback){
    readFile('data.json', (err, data) => {
        if(err){
            callback(err);
        } else {
            let group = data.employees.
            reduce((group, currentEmployee) => {

                if(!(currentEmployee.company in group)){
                    group[currentEmployee.company] = [];
                }
                group[currentEmployee.company].push(currentEmployee);

                return group;
            }, {});
            fs.writeFile(path.join(__dirname, 'problem2.json'), JSON.stringify(group, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else{
                    callback(null);
                }
            });
        } 
    });
}

function getDataPowerPuffBrigade(callback){
    readFile('problem2.json', (err, data) => {
        if(err){
            callback(err);
        }
        else {
            let foundData = [];
            if('Powerpuff Brigade' in data){
                foundData = data['Powerpuff Brigade'];
            }  
            fs.writeFile(path.join(__dirname, 'problem3.json'), JSON.stringify(foundData, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else{
                    callback(null);
                }
            });  
        }
    });
}

function removeId2(id, callback){
    readFile('data.json', (err, data) => {
        if(err){
            callback(err);
        }
        else {
            let filteredData = data.employees.filter((currentEmployee) => {
                return currentEmployee.id !== id;
            });
            fs.writeFile(path.join(__dirname, 'problem4.json'), JSON.stringify(filteredData, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else{
                    callback(null);
                }
            });
        }
    });
}

function sortByCompany(callback){
    readFile('data.json', (err, data) => {
        if(err){
            callback(err);
        }
        else{
            let sortedData = data.employees.sort((employee1, employee2) => {
                if(employee1.company === employee2.company){
                    return employee1.id - employee2.id;
                }
                else {
                    return employee1.company.localeCompare(employee2.company);
                }
            });
            fs.writeFile(path.join(__dirname, 'problem5.json'), JSON.stringify(sortedData, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else {
                    callback(null);
                }
            });
        }
    });
}

function swap(callback){
    readFile('data.json', (err,data) => {
        if(err){
            callback(err);
        }
        else {
            let index1 = data.employees.findIndex((currentEmployee) => {
                return currentEmployee.id === 93;
            });
            let index2 = data.employees.findIndex((currentEmployee) => {
                return currentEmployee.id === 92;
            });

            let temp = data.employees[index1];
            data.employees[index1] = data.employees[index2];
            data.employees[index2] = temp;

            fs.writeFile(path.join(__dirname, 'problem6.json'), JSON.stringify(data, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else {
                    callback(null);
                }
            });
        }
    });
}

function addBirthday(callback){
    readFile('data.json', (err, data) => {
        if(err){
            callback(err);
        }
        else {
            let result = data.employees.map((currentEmployee) => {
                if(currentEmployee.id % 2 === 0){
                    currentEmployee.birthday = new Date().toLocaleDateString();
                }
                return currentEmployee;
            });
            fs.writeFile(path.join(__dirname, 'problem7.json'), JSON.stringify(result, null, 4), (err) => {
                if(err){
                    callback(err);
                }
                else {
                    callback(null);
                }
            });
        }
    });
}

retrieveData([2, 13, 23], (err) => {
    if(err){
        console.error(err);
    }
    else {
        groupByCompanies((err, groupData) => {
            if(err){
                console.error(err);
            }
            else {
                getDataPowerPuffBrigade((err) => {
                    if(err){
                        console.error(err);
                    }
                    else{
                        removeId2(2, (err) => {
                            if(err){
                                console.error(err);
                            }
                            else {
                                sortByCompany((err) => {
                                    if(err){
                                        console.error(err);
                                    }
                                    else {
                                        swap((err) => {
                                            if(err){
                                                console.error(err);
                                            }
                                            else {
                                                addBirthday((err) => {
                                                    if(err){
                                                        console.error(err);
                                                    }
                                                    else {
                                                        console.log("All Done");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});




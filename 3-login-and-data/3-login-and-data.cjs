const fs = require("fs");
const path = require("path");

/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/

function createAndDeleteFiles(){
    let filenames = [];
    let writePromises = Array(2).fill(0).map((filename, index) => {
        filename = `file${index+1}.txt`;
        filenames.push(filename);
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, filename), 'File created', (err) => {
                if(err){
                    reject(err);
                }
                else{
                    resolve(`${filename} created`);
                }
            });
        });
    });

    Promise.all(writePromises)
    .then(() => {
        console.log("Files created");
    })
    .catch((err) => {
        console.error(err);
    });

    setTimeout(() => {
        let deletePromises = filenames.map((filename) => {
            return new Promise((resolve, reject) => {
                fs.unlink(path.join(__dirname, filename), (err) => {
                    if(err){
                        reject(err);
                    }
                    else{
                        resolve(`${filename} deleted`);
                    }
                });
            });
        });

        Promise.all(deletePromises)
        .then(() => {
            console.log("Files deleted");
        })
        .catch((err) => {
            console.error(err);
        });

    }, 2 * 1000);

}
   
createAndDeleteFiles();

/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function createAndDeleteLipsum(){

    let createLipsum = new Promise((resolve, reject) => {
        let data = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
        fs.writeFile(path.join(__dirname, 'lipsum.txt'), data, (err) => {
            if(err){
                reject(err);
            } else {
                resolve("Written To Lipsum.txt");
            }
        });
    })

    createLipsum.then(() => {
        let readLipsum = new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, 'lipsum.txt'), "utf-8" ,(err,data) => {
                if(err){
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        });
        return readLipsum;
    })
    .then((data) => {
        let writePromise =  new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, 'newLipsum.txt'), data, (err) => {
                if(err){
                    reject(err);
                } else {
                    resolve("Written to newLipsum.txt");
                }
            });
        });
        return writePromise;
    })
    .then(() => {
        let deletePromise = new Promise((resolve, reject) => {
            fs.unlink(path.join(__dirname, 'lipsum.txt'), (err) => {
                if(err){
                    reject(err);
                }
                else {
                    resolve("Deleted lipsum.txt");
                }
            });
        });
        return deletePromise;
    })
    .then((fromRes) => {
        console.log(fromRes);
    })
    .catch((err) => {
        console.error(err);
    });
}

createAndDeleteLipsum();



/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    let loggingData = {
        user,
        activity,
        timestamp : new Date().toString()
    }
    fs.writeFile(path.join(__dirname, 'logData.log'), JSON.stringify(loggingData, null, 2), {flag : "a"} ,(err) => {
        if(err){
            console.error(err);
        }
        else {
            console.log("Written to logData");
        }
    });
}

function logUser(user, val){
    login(user, val)
    .then((loginUser) => {
        logData(loginUser, "Login Success");
        return getData();
    })
    .then(() => {
        logData(user, "GetData Success");
    })
    .catch((err) => {
        if(err){
            logData(user, "Login Failure");
        } else {
            logData(user, "GetData Failure");
        }
    });
}

const user = {
    id : 3,
    name : "Ranjeet",
};

logUser(user, 3);
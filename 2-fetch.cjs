const fs = require("fs");
const path = require("path");
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const usersUrl = "https://jsonplaceholder.typicode.com/users";
const todosUrl = "https://jsonplaceholder.typicode.com/todos";

function fetchFromUrl(url){
    return new Promise((resolve, reject) => {
        fetch(url)
        .then((response) => {
            if(response.ok){
                resolve(response.json());
            }
            else{
                reject(response.status);
            }
        });
    })
}

function writeFile(filePath, data){
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, JSON.stringify(data, null, 4), (err) => {
            if(err){
                reject(err);
            }
            else{
                resolve(`Write to ${filePath} successfull`);
            }
        });
    });
}

//1. 
function fetchUsers(url){
    return new Promise((resolve, reject) => {
        fetchFromUrl(url)
        .then((data) => {
            return writeFile(path.join(__dirname, "usersData.json"), data);
        })
        .then((fromRes) => {
            resolve(fromRes);
        })
        .catch((err) => {
            reject(err);
        });
    });
}

//2. 
function fetchTodos(url){
    return new Promise((resolve, reject) => {
        fetchFromUrl(url)
        .then((data) => {
            return writeFile(path.join(__dirname, "todoData.json"), data);
        })
        .then((fromRes) => {
            resolve(fromRes);
        })
        .catch((err) => {
            reject(err);
        });
    });
}

//3.
function usersThenTodos(usersUrl, todosUrl){
    return new Promise((resolve, reject) => {
        fetchUsers(usersUrl)
        .then(() => {
            return fetchTodos(todosUrl);
        })
        .then((fromRes) => {
            resolve(fromRes);
        })
        .catch((err) => {
            reject(err);
        }); 
    });
}

//4.
function eachUser(usersUrl){
    return new Promise((resolve, reject) => {
        fetchFromUrl(usersUrl)
        .then((usersData) => {
            let eachUsersList = usersData.map((currentUser) => {
                return fetchFromUrl(`${usersUrl}?id=${currentUser.id}`)
            });

            return Promise.all(eachUsersList);
        })
        .then((eachUserData) => {
            return writeFile(path.join(__dirname, "eachUserData.json"), eachUserData);
        })
        .then((fromWrite) => {
            resolve(fromWrite);
        })
        .catch((err) => {
            reject(err);
        });
    });
}

//5.
function firstTodoUser(todosUrl){
    return new Promise((resolve, reject) => {
        fetchFromUrl(`${todosUrl}?id=1`)
        .then((todoData) => {
            return fetchFromUrl(`${usersUrl}?id=${todoData[0].userId}`)
        })
        .then((userData) => {
            return writeFile(path.join(__dirname, "firstTodoUser.json"), userData);
        })
        .then((fromWrite) => {
            resolve(fromWrite);
        })
        .catch((err) => {
            reject(err);
        });
    });
}


//calling Functions

fetchUsers(usersUrl)
.then((fromRes1) => {
    console.log(fromRes1);
    return fetchTodos(todosUrl);
})
.then((fromRes2) => {
    console.log(fromRes2);
    return usersThenTodos(usersUrl, todosUrl);
})
.then((fromRes3) => {
    console.log(fromRes3);
    return eachUser(usersUrl);
})
.then((fromRes4) => {
    console.log(fromRes4);
    return firstTodoUser(todosUrl)
})
.then((fromRes5) => {
    console.log(fromRes5);
})
.catch((err) => {
    console.error(err);
});
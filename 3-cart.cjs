const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 


function priceBeyond65(products){

    let productEntries = Object.entries(products[0]);
    let priceBeyond65Result = productEntries.reduce((productsArr, currentProduct) => {
        if(currentProduct[0] === 'utensils'){
            let utensilBeyond65 = currentProduct[1].filter((currentUtensil) => {
                let utensilItem = Object.entries(currentUtensil);
                return parseInt(utensilItem[0][1].price.substring(1)) > 65;
            });
            
            productsArr.push(...utensilBeyond65);

        }
        else if(parseInt(currentProduct[1].price.substring(1)) > 65){
            productsArr.push(Object.fromEntries([currentProduct]));
        }

        return productsArr;

    },[]);
    return priceBeyond65Result;
}

let priceBeyond65Result = priceBeyond65(products);

function quantityMoreThan1(products){
    
    let productEntries = Object.entries(products[0]);
    let quantityMoreThan1Result = productEntries.reduce((productsArr, currentProduct) => {
        if(currentProduct[0] === 'utensils'){
            let utensilBeyond1 = currentProduct[1].filter((currentUtensil) => {
                let utensilItem = Object.entries(currentUtensil);
                return utensilItem[0][1].quantity > 1;
            });
            productsArr.push(...utensilBeyond1);
        }
        else if(currentProduct[1].quantity > 1){
           
            productsArr.push(Object.fromEntries([currentProduct]));
        }

        return productsArr;

    },[]);
    return quantityMoreThan1Result;
}

let quantityMoreThan1Result =  quantityMoreThan1(products);

function fragileItems (products){
    let productEntries = Object.entries(products[0]);
    let fragileItemsResult = productEntries.reduce((productsArr, currentProduct) => {
        if(currentProduct[0] === 'utensils'){
            let fragileUtensils = currentProduct[1].filter((currentUtensil) => {
                let utensilItem = Object.entries(currentUtensil);
                if('type' in utensilItem[0][1]){
                    return utensilItem[0][1].type === 'fragile';
                } else {
                    return false;
                }
            });
            productsArr.push(...fragileUtensils);
        }
        else if('type' in currentProduct[1] && currentProduct[1].type === 'fragile'){
            productsArr.push(Object.fromEntries([currentProduct]));
        }

        return productsArr;

    },[]);
    return fragileItemsResult;
}

let fragileItemsResult = fragileItems(products);
let users = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

function findGender(users, gender){
    if(!Array.isArray(users) || typeof gender !== 'string'){
        return [];
    } else {
        let genderFound = users.filter((currentUser) => {
            return currentUser.gender.toLowerCase() === gender.toLowerCase();
        });
        return genderFound;
    }
}
let genderFound = findGender(users, 'agender');

function splitIp(users){
    if(!Array.isArray(users)){
        return [];
    }
    else{
        users.map((currentUser) => {
            return currentUser.ip_address = currentUser.ip_address.split('.');
        });
        return users;
    }
}

splitIp(users);

function findIpIndexSum(users, index){

    if(!Array.isArray(users) || index < 0 || index >= users.length){
        return -1;
    }
    else{

        let sum = users.reduce((sum, currentUser) => {
            sum += Number(currentUser.ip_address[index]);
            return sum;
        }, 0);

        return sum;
    } 
}

let sumIpSecond = findIpIndexSum(users, 1);

let sumIpFourth = findIpIndexSum(users, 3);
console.log(sumIpSecond);

function usersFullName(users){

    if(Array.isArray(users)){
        users.map((currentUser) => {
            return currentUser.full_name = `${currentUser.first_name} ${currentUser.last_name}`;
        });
        return users;
    }
}

usersFullName(users);

function filterEmail(users, filterValue){

    if(!Array.isArray(users) || typeof filterValue !== 'string'){
        return [];
    } else {
        let emailFound = users.filter((currentUser) => {
            return currentUser.email.endsWith(filterValue);
        }).reduce((emailArray, currentUser) => {
            emailArray.push(currentUser.email);
            return emailArray;
        }, []);

        return emailFound;
    }
}

let foundEmails = filterEmail(users, '.org');

function countEmails(users, ...matchDomains){

    let emailCounts ={};
    if(Array.isArray(users) || Array.isArray(matchDomains)){
        emailCounts = users.reduce((countEmail, currentUser) => {
            currentDomain = currentUser.email.split('.').slice(-1).toString();
            if(matchDomains.includes(currentDomain)){
                if(!(currentDomain in countEmail)){
                    countEmail[currentDomain] = 0;
                }
                countEmail[currentDomain]++;
            }
            return countEmail;
        }, {});

        matchDomains.forEach((domain) => {
            if(!(domain in emailCounts)){
                emailCounts[domain] = 0;
            }
        });
    }
    return emailCounts;
}

let emailCountResult = countEmails(users, 'com', 'au', 'org');

function sortFirstNameDecending(users){
    if(Array.isArray(users)){
        users.sort((firstUser, secondUser) => {
            if(firstUser.first_name < secondUser.first_name){
                return 1;
            }
            else if(firstUser.first_name > secondUser.first_name){
                return -1;
            }
            else{
                if(firstUser.last_name < secondUser.last_name){
                    return 1;
                }
                else if(firstUser.last_name > secondUser.last_name){
                    return -1;
                } else{
                    return 0;
                }    
            }  
        });
    }
}
sortFirstNameDecending(users);
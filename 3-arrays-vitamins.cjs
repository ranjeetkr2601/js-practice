const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

//1. Get all items that are available 
function available(items){

    if(Array.isArray(items)){
        let availableResult = items.filter((item) => {
            return item.available;
        });
        return availableResult;
    }
    else{
        return {};
    }
}

let availableResult = available(items);

//2. Get all items containing only Vitamin C.

function getVitamin(items, vitamin){
    if(Array.isArray(items)){
        let vitaminResult = items.filter((item) => {
            return item.contains.includes(vitamin);
        });
        return vitaminResult;
    }
    else{
        return {};
    }
}

//2. Get all items containing only Vitamin C.
let vitaminC = getVitamin(items, 'Vitamin C')
    .filter((item) => {
        return item.contains === 'Vitamin C';
});

// 3. Get all items containing Vitamin A.
let vitaminA = getVitamin(items, 'Vitamin A');

// 4. Group items based on the Vitamins that they contain
function groupVitamin(items){
    if(Array.isArray(items)){
        let groupResult = items.reduce((group, item) => {
            item.contains.split(', ')
              .map((vitamin) => {
                if(!(vitamin in group)){
                    group[vitamin] = [];
                }
                group[vitamin].push(item.name);
            });
            return group;
        }, {});

        return groupResult;
    }
}

let groupVitaminResult = groupVitamin(items);

// 5. Sort items based on number of Vitamins they contain.
function sortByVitamins(items){
    if(Array.isArray(items)){
        items.sort((firstItem, secondItem) => {
            return firstItem.contains.split(',').length - secondItem.contains.split(',').length;
        })
    }
}

sortByVitamins(items);
const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

//1.
function earningsMoreThan500M(favMovies){
    let moviesEntries = Object.entries(favMovies);
    let foundEntries = moviesEntries.filter((currentMovie) => {
        return parseInt(currentMovie[1].totalEarnings.substring(1)) > 500;
    });
    return Object.fromEntries(foundEntries);
}

let moviesEarningBeyond500M = earningsMoreThan500M(favouritesMovies);

//2.
function oscarsMoreThan3(favMovies){
    let moviesBeyond500M = earningsMoreThan500M(favMovies);
    let moviesBeyond500MEntries = Object.entries(moviesBeyond500M);
    let oscarsMoreThan3 = moviesBeyond500MEntries.filter((currentMovie) => {
        return currentMovie[1].oscarNominations > 3;
    });

    return Object.fromEntries(oscarsMoreThan3);
}

let oscarsMoreThan3Result = oscarsMoreThan3(favouritesMovies);

//3.
function findMoviesOfActor(favMovies, actor){

    let favMoviesEntries = Object.entries(favMovies);

    let actorsMovies = favMoviesEntries.filter((currentMovie) => {
        return currentMovie[1].actors.includes(actor);
    });

    return Object.fromEntries(actorsMovies);
}

let moviesOfLeonardo = findMoviesOfActor(favouritesMovies, 'Leonardo Dicaprio');

//4. 
function sortByImdb(favMovies){

    let favMoviesEntries = Object.entries(favMovies);
    favMoviesEntries.sort((movie1, movie2) => {
        let imdb1 = movie1[1].imdbRating;
        let imdb2 = movie2[1].imdbRating;
        let earning1 = parseInt(movie1[1].totalEarnings.substring(1));
        let earning2 = parseInt(movie2[1].totalEarnings.substring(1));

        if(imdb1 !== imdb2){
            return imdb2 - imdb1;
        }
        else{
            return earning2 - earning1;
        }
    });

    return Object.fromEntries(favMoviesEntries);

}

let sortedMoviesByImdb = sortByImdb(favouritesMovies);

//5.
function groupByGenre(favMovies){

    function getMaxPriority(genres){
        let genresOrder = ["crime", "thriller", "adventure", "sci-fi", "drama"];
        let maxGenreIndex = genres.reduce((genreMaxIndex, currentGenre) => {
            let indexOfGenre = genresOrder.indexOf(currentGenre);
            if(indexOfGenre > genreMaxIndex){
                genreMaxIndex = indexOfGenre;
            }
            return genreMaxIndex;
        }, 0);
        return genresOrder[maxGenreIndex];
    }

    let favMoviesEntries = Object.entries(favMovies);
    
    let groupByGenre = favMoviesEntries.reduce((group, currentMovie) => {
        let maxGenre = getMaxPriority(currentMovie[1].genre);
        if(!(maxGenre in group)){
            group[maxGenre] = [];
        }
        group[maxGenre].push(currentMovie[0]);
        return group;
    }, {});
    return groupByGenre;
}

let groupedByGenre = groupByGenre(favouritesMovies);

const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

//1.
function findInterest(users, interest){
    let usersKeys = Object.keys(users);
    let foundUsers = usersKeys.filter((currentUser) => {

        if('interest' in users[currentUser]){
            return (users[currentUser].interest).toString().includes(interest);
        }
        else if('interests' in users[currentUser]){
            return (users[currentUser].interests).toString().includes(interest);
        } 
        else {
            return false;
        }
    });
    return foundUsers;
}

foundVideoGamesUsers = findInterest(users,'Video Games');

//2.
function findNationality(users, country){
    let usersEntries = Object.entries(users);
    let foundUserEntries = usersEntries.filter((currentUser) => {
        return currentUser[1].nationality === country;
    });

    return Object.fromEntries(foundUserEntries);
}

let foundUsersCountry = findNationality(users,'Germany');

//3.
function sortByDesignationAndAge(users){

    function findDesignation(user){
        if(user.desgination.includes("Senior")){
            return 3;
        }
        else if(user.desgination.includes("Developer")){
            return 2;
        }
        else{
            return 1;
        }
    }

    let usersEntries = Object.entries(users);
    usersEntries.sort((user1, user2) => {
        let designation1 = findDesignation(user1[1]);
        let designation2 = findDesignation(user2[1]);
        let age1 = user1[1].age;
        let age2 = user2[1].age;
        if(designation1 < designation2){
            return 1;
        }
        else if(designation1 > designation2){
            return -1;
        }
        else {
            return age2 - age1;
        }
    });

    return Object.fromEntries(usersEntries);
}

let sortedByDesginationAndAge =  sortByDesignationAndAge(users);

//4.
function findMasters(users){
    let usersEntries = Object.entries(users);
    let foundMasters = usersEntries.filter((currentUser) => {
        return currentUser[1].qualification === "Masters";
    });
    
    return Object.fromEntries(foundMasters);
}

let foundMasters = findMasters(users);


//5.
function groupByLanguage(users){

    function getLanguage(user){
        let allLanguages = ['Javascript', 'Golang', 'Python'];
        return allLanguages.find((currentLanguage) => {
            return (user.desgination.includes(currentLanguage));
        });   
    }

    let userEntries = Object.entries(users);
    let groupedByLanguage = userEntries.reduce((group, currentUser) => {
        let language = getLanguage(currentUser[1]);
        if(!(language in group) && language !== 'undefined'){
            group[language] = [];
        }
        group[language].push(currentUser[0]);
        return group;
    }, {});
    return groupedByLanguage;
}

let groupedByLanguage = groupByLanguage(users);